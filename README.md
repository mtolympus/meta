# Laravel Meta

This package provides a simple way to store key-value based data in the database.

# Installation

Require the package in your application by running the following command:
```
composer require hermes/meta
```

You're done!

# Usage

The following methods are available:

- get($key)
- getValue($key)
- set($key, $value)

Check the Meta.php file for more method descriptions.