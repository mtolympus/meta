<?php

namespace Hermes\Meta\Models;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $table = "metas";
    protected $guarded = ["id", "created_at", "updated_at"];
    protected $fillable = ["key", "value"];
}