<?php

namespace Hermes\Meta;

use Hermes\Meta\Models\Meta as MetaModel;

class Meta
{
    /**
     * Meta package
     * 
     * @author Nick Verheijen <verheijen.webdevelopment@gmail.com>
     * @version 1.0.0
     */
    public function __construct()
    {

    }

    /**
     * Get
     * 
     * @param       string              The key of the meta
     * @return      App\Models\Meta     The meta or false if it does not exist
     */
    public function get($key)
    {
        return MetaModel::where("key", $key)->first();
    }

    /**
     * Get meta's value
     * 
     * @param       string              The key of the meta
     * @return      string              The value of the meta
     */
    public function getValue($key)
    {
        // Attempt to find the meta by it's key
        $meta = $this->get($key);
        
        // If we retrieved it
        if ($meta)
        {
            // Return it's value
            return $meta->value;
        }

        // If it did not exist, return false
        return false;
    }

    /**
     * Set meta's value
     * 
     * @param       string              The key of the meta we want to set the value of
     * @param       string              The value we want to set
     * @return      App\Models\Meta     The updated (or created) meta
     */
    public function set($key, $value)
    {
        // Attempt to find the meta by it's key
        $meta = MetaModel::where("key", $key)->first();

        // If we found it
        if ($meta)
        {
            // Update it's value
            $meta->value = $value;
            $meta->save();
        }
        // If it does not exist
        else
        {
            // Create it
            $meta = MetaModel::create(["key" => $key, "value" => $value]);
        }

        // Return the Meta object
        return $meta;
    }
}