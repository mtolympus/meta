<?php

namespace Hermes\Meta\Facades;

use Illuminate\Support\Facades\Facade;

class Meta extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "meta";
    }
}