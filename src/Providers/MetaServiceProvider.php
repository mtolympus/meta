<?php

namespace Hermes\Meta\Providers;

use Hermes\Meta\Meta;
use Illuminate\Support\ServiceProvider;

class MetaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Setup migration loading
        $this->loadMigrationsFrom(__DIR__."/../Database/migrations");

        // Setup migration publishing in case modifications to the database structure need to be made.
        $this->publishes([__DIR__."/../Database/migrations" => database_path("migrations")], "migrations");
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register the Settings service as a singleton to the IoC
        $this->app->singleton("meta", function() {
            return new Meta;
        });
    }
}